package classes;

import java.util.Objects;

public class Simptom {
        private int id;
        private String description;

        public Simptom() {};
        public Simptom(int id, String description) {
        this.id = id;
        this.description = description;
            }

      //  public Simptom(int id, String name, String surname, String sex, int age, String data, int price, String specialization) {
       //     super(id, name, surname, sex, age, data, price, specialization);}

    public int getId() {
        return id;
    }
    public String getDescription () {
        return description;
    }
   // public ArrayList<Psychologist> getPsychologist() {  return specialization; }
    public void setId(String psychSimptom) {
        this.id = id;
    }
    public void setDescription(String description) {
        this.description = description;
    }
   // public void setPsychologist(ArrayList<Psychologist> specialization) {this.specialization = specialization;}

    //Метод с переменным количеством аргументов
   // public void createArrayOfPS(Psychologist...p){
   //     for (Psychologist i : p)
   //         specialization.add(i);}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Simptom)) return false;
        Simptom simptom = (Simptom) o;
        return id == simptom.id &&
                Objects.equals(description, simptom.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, description);
    }

    @Override
    public String toString() {
        return "Simptom{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
