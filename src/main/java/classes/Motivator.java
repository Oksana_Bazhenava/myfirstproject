package classes;
import java.util.Objects;

public class Motivator {
    private int id;
    private String sex;
    private String motivText;
   // ArrayList<Motivator> manMotivators;
   // ArrayList<Motivator> womanMotivators;

    public Motivator () {};

    public Motivator (int id, String sex, String motivText) {
        this.id = id;
        this.sex = sex;
        this.motivText = motivText;
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMotivText() {
        return motivText;
    }

    public void setMotivText(String motivText) {
        this.motivText = motivText;
    }

    public void printData() {
        System.out.println("ID мотиватора: " + id);
        System.out.println("Пол: " + sex);
        System.out.println("Текст: " + motivText);
            }
    @Override
    public String toString() {
        return "Motivator{" +
                "id=" + id +
                ", sex='" + sex + '\'' +
                ", motivText='" + motivText + '\'' +
                '}';
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.sex);
        hash = 83 * hash + Objects.hashCode(this.motivText);
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Motivator other = (Motivator) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.sex != other.sex) {
            return false;
        }
        if (!Objects.equals(this.motivText, other.motivText)) {
            return false;
        }
        return true;
    }

}
