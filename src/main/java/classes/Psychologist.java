package classes;

import java.util.ArrayList;
import java.util.Objects;

public class Psychologist {
    private int id;
    private String name;
    private String surname;
    private String sex;
    private int age;
    private String data;//данные о психологе -- анкета
    private int price;//стоимость услуг
    transient ArrayList<Simptom> specialization = new ArrayList<Simptom>();//специализация по симптомам - список симптомов

    public Psychologist() {};

    public Psychologist(int id, String name, String surname, String sex, int age, String data, int price) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.data = data;
        this.price = price;
                   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<Simptom> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(ArrayList<Simptom> specialization) {
        this.specialization = specialization;
    }

    //Метод с переменным количеством аргументов
    public void createArrayOfPS(Simptom...s){
        for (Simptom i : s)
            specialization.add(i);}

    public void printData() {
        System.out.println("ID психолога: " + id);
        System.out.println("Фамилия: " + name);
        System.out.println("Имя: " + surname);
        System.out.println("Пол: " + sex);
        System.out.println("Возраст: " + age);
        System.out.println("Даные о психологе: " + data);
        System.out.println("Стоимость услуги за 1 сессию (50 мин): " + price + "$");
        }

    @Override
    public String toString() {
        return "Psychologis{" + "name=" + name + ", surname=" + surname + ", id=" + id + ", sex=" + sex + ", age=" + age + ", data=" + data + ", price=" + price + ", specialization=" + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.surname);
        hash = 83 * hash + Objects.hashCode(this.sex);
        hash = 83 * hash + this.age;
        hash = 83 * hash + Objects.hashCode(this.data);
        hash = 83 * hash + Objects.hashCode(this.price);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Psychologist other = (Psychologist) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.sex, other.sex)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }
}



