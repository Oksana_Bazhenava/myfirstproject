package classes;

import java.util.Objects;

public class Article {
    private int id;
    private String name;
    private String author;
    private String psychSimptomn;
    private String text;//текст статьи

    public Article () {};

    public Article (int id, String name, String author, String psychSimptomn, String text) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.psychSimptomn = psychSimptomn;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPsychSimptomn() {
        return psychSimptomn;
    }

    public void setPsychSimptomn(String psychSimptomn) {
        this.psychSimptomn = psychSimptomn;
    }

    public String getText() {
        return text;
    }

    public void setArticle(String article) {
        this.text = text;
    }

    public void printData() {
        System.out.println("ID статьи: " + id);
        System.out.println("Наименование статьи: " + name);
        System.out.println("Автор: " + author);
        System.out.println("Симптомы: " + psychSimptomn);
        System.out.println("Текст: " + text);
    }

    @Override
    public String toString() {
        return "SelfHelp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", psychSimptomn='" + psychSimptomn + '\'' +
                ", article='" + text + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        Article article = (Article) o;
        return id == article.id &&
                Objects.equals(name, article.name) &&
                Objects.equals(author, article.author) &&
                Objects.equals(psychSimptomn, article.psychSimptomn) &&
                Objects.equals(article, article.text);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.author);
        hash = 83 * hash + Objects.hashCode(this.psychSimptomn);
        hash = 83 * hash + Objects.hashCode(this.text);
        return hash;
    }
}
