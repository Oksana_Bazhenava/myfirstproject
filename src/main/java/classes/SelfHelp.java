package classes;

import java.util.Objects;

public class SelfHelp {
    private int id;
    private String name;
    private String psychSimptomn;
    private String exercise;//текст упражнения

    public SelfHelp () {};

    public SelfHelp (int id, String name, String psychSimptomn, String exercise) {
        this.id = id;
        this.name = name;
        this.psychSimptomn = psychSimptomn;
        this.exercise = exercise;
       }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPsychSimptomn() {
        return psychSimptomn;
    }

    public void setPsychSimptomn(String psychSimptomn) {
        this.psychSimptomn = psychSimptomn;
    }

    public String getArticle() {
        return exercise;
    }

    public void setArticle(String exercise) {
        this.exercise = exercise;
    }

    public void printData() {
        System.out.println("ID упражнения самопомощи: " + id);
        System.out.println("Наименование упражнения: " + name);
        System.out.println("Симптомы: " + psychSimptomn);
        System.out.println("Содержание: " + exercise);
        }

    @Override
    public String toString() {
        return "SelfHelp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", psychSimptomn='" + psychSimptomn + '\'' +
                ", exercise='" + exercise + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SelfHelp)) return false;
        SelfHelp selfHelp = (SelfHelp) o;
        return id == selfHelp.id &&
                Objects.equals(name, selfHelp.name) &&
                Objects.equals(psychSimptomn, selfHelp.psychSimptomn) &&
                Objects.equals(exercise, selfHelp.exercise);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.psychSimptomn);
        hash = 83 * hash + Objects.hashCode(this.exercise);
        return hash;
    }
}
