package classes;

import java.util.ArrayList;

public class PsychHelp {
    ArrayList<Simptom> specialization = new ArrayList<Simptom>();
    ArrayList<Motivator> womanMotivators = new ArrayList<Motivator>();
    ArrayList<Motivator> manMotivators = new ArrayList<Motivator>();
    ArrayList<User> users = new ArrayList<User>();
    ArrayList<Article> articles = new ArrayList<Article>();
    ArrayList<Psychologist> psychologists = new ArrayList<Psychologist>();
    ArrayList<Test> tests = new ArrayList<Test>();
    ArrayList<SelfHelp> selfHelps = new ArrayList<SelfHelp>();


    //Метод с переменным количеством аргументов
    public void addToSimp(Simptom...s){
        for (Simptom i : s)
            specialization.add(i);}

    public void addToWomenMot(Motivator...m){
        for (Motivator i : m)
            womanMotivators.add(i);}

    public void addToManMot(Motivator...m){
        for (Motivator i : m)
            manMotivators.add(i);}

    public void addToUsers(User...u){
        for (User i : u)
            users.add(i);}

    public void addToArticles(Article...a){
        for (Article i : a)
            articles.add(i);}

    public void addToPsych(Psychologist...p){
        for (Psychologist i : p)
            psychologists.add(i);}

    public void addToTests(Test...t){
        for (Test i : t)
            tests.add(i);}

    public void addToSelfHelp(SelfHelp...s){
        for (SelfHelp i : s)
            selfHelps.add(i);}

    public ArrayList<Simptom> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(ArrayList<Simptom> specialization) {
        this.specialization = specialization;
    }

    public ArrayList<Motivator> getWomanMotivators() {
        return womanMotivators;
    }

    public void setWomanMotivators(ArrayList<Motivator> womanMotivators) {
        this.womanMotivators = womanMotivators;
    }

    public ArrayList<Motivator> getManMotivators() {
        return manMotivators;
    }

    public void setManMotivators(ArrayList<Motivator> manMotivators) {
        this.manMotivators = manMotivators;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    public ArrayList<Psychologist> getPsychologists() {
        return psychologists;
    }

    public void setPsychologists(ArrayList<Psychologist> psychologists) {
        this.psychologists = psychologists;
    }

    public ArrayList<Test> getTests() {
        return tests;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public ArrayList<SelfHelp> getSelfHelps() {
        return selfHelps;
    }

    public void setSelfHelps(ArrayList<SelfHelp> selfHelps) {
        this.selfHelps = selfHelps;
    }
}
