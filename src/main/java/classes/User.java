package classes;

import java.util.Objects;

public class User {

    private int id;
    private String name;
    private String surname;
    private String sex;
    private int age;
    private String date;//дата регистрации
    private String password;

    //List<User> clients = new ArrayList<User>();

    public User() {};
    public User(int id, String name, String surname, String sex, int age, String date, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.date = date;
        this.password = password;
    }

    public void setId(int ID) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTypeProcedure(String TypeProcedure) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public String getDate() {
        return date;
    }

    public String getPassword() {
        return password;
    }

    public void printData() {
        System.out.println("ID клиента: " + id);
        System.out.println("Имя: " + name);
        System.out.println("Фамилия: " + surname);
        System.out.println("Пол: " + sex);
        System.out.println("Возраст: " + age);
        System.out.println("Дата регистрации: " + date);
        System.out.println("Пороль: " + password);
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", surname=" + surname + ", id=" + id + ", sex=" + sex + ", age=" + age + ", date=" + date + ", password=" + password + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.surname);
        hash = 83 * hash + Objects.hashCode(this.sex);
        hash = 83 * hash + this.age;
        hash = 83 * hash + Objects.hashCode(this.date);
        hash = 83 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.sex, other.sex)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }

}