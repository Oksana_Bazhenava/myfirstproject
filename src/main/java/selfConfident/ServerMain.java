package selfConfident;


import classes.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;

import static selfConfident.DbHelper.getDBConnection;

public class ServerMain {
    private static ServerMain server;
    private OutputStream sout;
    private InputStream sin;
    private DataInputStream in;
    private DataOutputStream out;
    private PrintWriter writer;
    private ObjectOutputStream objOStrm;
    private ObjectInputStream objIStrm;

    public static void main(String[] args) {
        server = new ServerMain();
        server.start();
        server = new ServerMain();
        //server.init();
        server.start();

    }

    private void init() {
        //Создание объектов класса (с использованием методов set или параметризированного конструктора)
        // добавление этих объектов в коллекцию
        // public Psychologist(int id, String name, String surname, String sex, int age, String data, int price, String specialization)
        //ПСИХОЛОГИ
        PsychHelp help = new PsychHelp();
        Psychologist p1 = new Psychologist(1, "Королев", "Дмитрий", "мужчина", 35, "Работает психологом с 2005 года", 20);
        Psychologist p2 = new Psychologist(2, "Созанович", "Иван", "мужчина", 35, "Работает психологом с 2005 года", 20);
        Psychologist p3 = new Psychologist();//конструктор без параметров
        p3.setId(3);//Инициализация статической переменной класса
        p3.setName("Королев");
        p3.setSurname("Константин");
        p3.setSex("мужчина");
        p3.setAge(60);
        p3.setData("Действующий психотерапевт с 1990 года");
        p3.setPrice(40);
        System.out.println("ИНФОРМАЦИЯ О ПСИХОЛОГАХ: ");
        p1.printData();
        p2.printData();
        p3.printData();
        help.addToPsych(p1, p2, p3);

        //СИМПТОМЫ
        Simptom s1 = new Simptom(1, "Переживание горя");
        Simptom s2 = new Simptom(2, "Личностный рост");
        Simptom s3 = new Simptom(3, "Стресс");
        Simptom s4 = new Simptom(4, "Развод");
        Simptom s5 = new Simptom(5, "Достижение цели");
        Simptom s6 = new Simptom(6, "Сильная зависимость");
        p1.createArrayOfPS(s2, s5);
        help.addToSimp(s1, s2, s3, s4, s5, s6);
        System.out.println(help.getSpecialization());

        System.out.println("СПИСОК СИМПТОМОВ ДЛЯ ПСИХОЛОГА: " + p1.getName());
        System.out.println(p1.getSpecialization());
        p2.createArrayOfPS(s1, s2, s4);
        System.out.println("СПИСОК СИМПТОМОВ ДЛЯ ПСИХОЛОГА: " + p2.getName());
        System.out.println(p2.getSpecialization());
        p3.createArrayOfPS(s6, s2, s5);
        System.out.println("СПИСОК СИМПТОМОВ ДЛЯ ПСИХОЛОГА: " + p3.getName());
        System.out.println(p3.getSpecialization());

        //Добавление списка симптомов к психологам
        //p1.addSpecialization(s2,  s5);

        //МОТИВАТОРЫ
        Motivator m1 = new Motivator(1, "жен", "Вы прекрасны!");
        Motivator m2 = new Motivator(2, "жен", "У Вас все получится!");
        Motivator m3 = new Motivator(3, "жен", "Вы сильнее, чем думаете!");
        Motivator m4 = new Motivator(4, "муж", "Do what you can, with what you have, where you are. Theodore Roosevelt!");
        Motivator m5 = new Motivator(5, "муж", "Живи так, чтобы тобой гордились!");
        Motivator m6 = new Motivator(5, "муж", "Не сдавайтесь!");
        System.out.println("СПИСОК МОТИВАТОРОВ ДЛЯ ЖЕНЩИН: ");
        m1.printData();
        m2.printData();
        m3.printData();
        System.out.println("СПИСОК МОТИВАТОРОВ ДЛЯ МУЖЧИН: ");
        m4.printData();
        m5.printData();
        help.addToWomenMot(m1, m2, m3);
        help.addToWomenMot(m4, m5, m6);


        //ПОЛЬЗОВАТЕЛИ
        User u1 = new User(1, "Корташов", "Андрей", "мужчина", 25, "12.04.2018", "159456");
        User u2 = new User(2, "Малышева", "Алина", "женщина", 28, "12.02.2018", "784523");
        System.out.println("СПИСОК КЛИЕНТОВ: ");//?????????????????????????
        u1.printData();
        u2.printData();
        help.addToUsers(u1, u2);

    }

    private void closeStreams() throws IOException {
        sin.close();
        sout.close();
        in.close();
        out.close();
        writer.close();
        objIStrm.close();
        objOStrm.close();
    }

    private void initStreams(Socket socket) throws IOException {
        // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиенту.
        sin = socket.getInputStream();
        sout = socket.getOutputStream();

        // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
        in = new DataInputStream(sin);
        out = new DataOutputStream(sout);
        writer = new PrintWriter(socket.getOutputStream(), true);

        objOStrm = new ObjectOutputStream(sout);
        objIStrm = new ObjectInputStream(sin);
    }

    public void start() {
        int port = 6666;
        // создаем сокет сервера и привязываем его к вышеуказанному порту
        try (ServerSocket ss = new ServerSocket(port)) {

            System.out.println("Ожидается подключение клиента...");
            Socket socket = ss.accept(); // заставляем сервер ждать подключений и выводим сообщение когда кто-то связался с сервером
            System.out.println("Клиент подключен");

            initStreams(socket);
            Connection con = getDBConnection();

            int choice = 0, a = 0;
            String line;
            while (a == 0) {
                //получили от клиента строку
                line = (String) objIStrm.readObject();
                switch (line) {
                    case "m":
                    case "м":
                    case "М":
                    case "M": {
                        ArrayList<String> motivators = DbHelper.showMotivator("М", con);
                        String mot;
                        Collections.shuffle(motivators);
                        mot = (String) motivators.get(0);
                        objOStrm.writeObject(mot);
                        System.out.println(mot);
                        break;
                    }
                    case "ж":
                    case "Ж": {
                        ArrayList<String> motivators = DbHelper.showMotivator("Ж", con);
                        String mot;
                        Collections.shuffle(motivators);
                        mot = (String) motivators.get(0);
                        objOStrm.writeObject(mot);
                        System.out.println(mot);
                        break;
                    }

                    default: {

                        break;
                    }
                }
            }

            System.out.println();
            closeStreams();

        } catch (IOException x) {
            System.out.println("Некорректный ввод!");
            x.printStackTrace();
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

}
