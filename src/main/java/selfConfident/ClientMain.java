package selfConfident;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class ClientMain {
    private static ObjectOutputStream objOStrm;
    private static ObjectInputStream objIStrm;
    private static OutputStream sout;
    private static InputStream sin;
    private static DataInputStream in;
    private static DataOutputStream out;

    public static void main(String[] ar) {
        int serverPort = 6666;        //  указываем порт, к которому привязывается сервер
        String address = "127.0.0.1"; // IP-адрес компьютера, где исполняется серверная программа
        // (здесь указан адрес того самого компьютера где будет исполняться и клиент)

        try {
            InetAddress ipAddress = InetAddress.getByName(address); // создаем объект который отображает вышеописанный IP-адрес
            Socket socket = new Socket(ipAddress, serverPort);      // создаем сокет используя IP-адрес и порт сервера.

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом.
            sin = socket.getInputStream();
            sout = socket.getOutputStream();

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
            in = new DataInputStream(sin);
            out = new DataOutputStream(sout);

            // Потоки для чтения и передачи объектов
            objIStrm = new ObjectInputStream(sin);
            objOStrm = new ObjectOutputStream(sout);

            boolean isContinue = true;
            String line = null;
            try {
                while (isContinue) {
                    System.out.println("Укажите Ваш пол (М/Ж): ");
                    System.out.println("0 - Выход");
                    //Cчитываем с клавиатуры пользовательский ввод (как строку)
                    BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
                    line = keyboard.readLine();
                    switch (line) {
                        case "0":
                            isContinue = false;
                            break;
                        case "m":
                        case "м":
                        case "М":
                        case "M":
                            objOStrm.writeObject(line);
                            String mMan = (String) objIStrm.readObject();
                            System.out.println(mMan);
                            break;
                        case "ж":
                        case "Ж":
                            objOStrm.writeObject(line);
                            String mWom = (String) objIStrm.readObject();
                            System.out.println(mWom);
                            break;
                        default: {
                            System.out.println("Некорректный ввод, попробуйте еще раз!");
                            break;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            sin.close();
            sout.close();
            in.close();
            out.close();
            objOStrm.close();
            objIStrm.close();

        } catch (IOException x) {
            System.out.println("Некорректный ввод!");
            x.printStackTrace();
        } catch (Exception x) {
            x.getMessage();
        }

    }

}