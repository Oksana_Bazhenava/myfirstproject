package selfConfident;

import java.sql.*;
import java.util.ArrayList;

public class DbHelper {
    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";

    public static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/self";

    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";


    public static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Соединение с базой данных установлено!");
        return dbConnection;

    }

    public static ArrayList<String> showMotivator(String s, Connection con) {
        String query;
        ResultSet rs = null;
        ArrayList<String> motivators = new ArrayList<>();
        if (s == "М") {
            query = "SELECT * FROM motivator WHERE sex='mug'";
            try {
                Statement stmt = con.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    String text = rs.getString(3);
                    motivators.add(text);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
            if (s == "Ж") {
                query = "SELECT * FROM motivator WHERE sex='gen'";
                try {
                    Statement stmt = con.createStatement();
                    rs = stmt.executeQuery(query);
                    while (rs.next()) {
                        String text = rs.getString(3);
                        motivators.add(text);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return motivators;
    }
}

